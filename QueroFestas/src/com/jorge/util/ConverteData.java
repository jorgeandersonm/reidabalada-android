package com.jorge.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ConverteData {
	
	public static String dataParaString(Date date){
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		return format.format(date);
	}
	
	public static Date stringParaDate(String string){
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Date data;
		
		try {
			data = (Date)formatter.parse(string);
		} catch (ParseException e) {
			return null;
		}
		return data;
	}

	public static String formatarData(String string){
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		
		return formatter.format(string);
	}
}
