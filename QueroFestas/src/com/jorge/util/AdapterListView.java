package com.jorge.util;

import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.jorge.model.Evento;
import com.jorge.querofestas.R;


public class AdapterListView extends BaseAdapter
{
    private LayoutInflater mInflater;
    private List<Evento> itens;
    Typeface type;
    Typeface type2;
    TextView textoNome;
    TextView textoData;
 
    public AdapterListView(Context context, List<Evento> itens)
    {
        //Itens que preencheram o listview
        this.itens = itens;
        //responsavel por pegar o Layout do item.
        mInflater = LayoutInflater.from(context);
        
        
        
        type = Typeface.createFromAsset(context.getAssets(),
                "fonts/OpenSans-Light.ttf");
        
        type2 =Typeface.createFromAsset(context.getAssets(),
                "fonts/OpenSans-Bold.ttf");

    }
 
    /**
     * Retorna a quantidade de itens
     *
     * @return
     */
    public int getCount()
    {
        return itens.size();
    }
 
    /**
     * Retorna o item de acordo com a posicao dele na tela.
     *
     * @param position
     * @return
     */
    public Evento getItem(int position)
    {
        return itens.get(position);
    }
 
    /**
     * Sem implementa��o
     *
     * @param position
     * @return
     */
    public long getItemId(int position)
    {
        return position;
    }
 
    public View getView(int position, View view, ViewGroup parent)
    {
        //Pega o item de acordo com a pos��o.
    	Evento item = itens.get(position);
        //infla o layout para podermos preencher os dados
        view = mInflater.inflate(R.layout.festa, null);
 
        //atravez do layout pego pelo LayoutInflater, pegamos cada id relacionado
        //ao item e definimos as informa��es.
        ((TextView) view.findViewById(R.id.textoLista)).setText(item.getNome());
        ((TextView) view.findViewById(R.id.textoLista)).setTypeface(type2);
        ((TextView) view.findViewById(R.id.dataList)).setText(item.getData());
        ((TextView) view.findViewById(R.id.dataList)).setTypeface(type);

        return view;
    }
}