package com.jorge.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import com.jorge.model.Evento;

public class FestasPorData {

	public static List<Evento> filtrarFestasPorData(String dataInicio, String dataFim, List<Evento> objFesta){
		
		List<Evento> festasFiltradas = new ArrayList<Evento>();

		if(dataFim == null){			

			for(int i = 0; i < objFesta.size(); i++){
				if(dataInicio.equals(objFesta.get(i).getData())){
					festasFiltradas.add(objFesta.get(i));
				}
			}
			return festasFiltradas;
		}
		else{

			Date dataStart = ConverteData.stringParaDate(dataInicio);
			Date dataEnd = ConverteData.stringParaDate(dataFim);



			List<String> dates = new ArrayList<String>();
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(dataStart);

			while (calendar.getTime().before(dataEnd))
			{
				Date resultado = calendar.getTime();
				dates.add(ConverteData.dataParaString(resultado));
				calendar.add(Calendar.DATE, 1);
			}

			for(int i = 0; i < objFesta.size(); i++){
				for(int j = 0; j < dates.size(); j++){
					if(objFesta.get(i).getData().equals(dates.get(j))){
						festasFiltradas.add(objFesta.get(i));
					}
				}
			}
		}
		return festasFiltradas;
	}
}
