package com.jorge.querofestas;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;



@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class MapaActivity extends Activity{

	private static LatLng LOCALIZACAO;
	private String local;
	private String nome;
	private GoogleMap googleMap;
	private String lat;
	private String lon;

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapa);
 
        try {
            // Loading map
        	pegarDados();
            initilizeMap();
 
        } catch (Exception e) {
            e.printStackTrace();
        }
		            
//		mapView.moveCamera( CameraUpdateFactory.newLatLngZoom( LOCALIZACAO, 15 ) );
//		mapView.addMarker(new MarkerOptions()
//		.position( LOCALIZACAO )
//		.title(local)
//		.snippet(nome) );
	}

	public void pegarDados(){
		Bundle extras = getIntent().getExtras();
		Double lat1 = Double.parseDouble(extras.getString("lat"));
		Double lon1 = Double.parseDouble(extras.getString("lon"));
		
		lat = extras.getString("lat");
		lon = extras.getString("lon");

		local = extras.getString("local");
		nome = extras.getString("nome");
		

		LOCALIZACAO = new LatLng( lat1, lon1);
	}

    private void initilizeMap() {
        if (googleMap == null) {
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(
                    R.id.map)).getMap();
            
            googleMap.setMyLocationEnabled(true);
             
            // create marker
            MarkerOptions marker = new MarkerOptions().position(LOCALIZACAO).title(local).snippet(nome);
             
            // adding marker
            googleMap.addMarker(marker);
            
            CameraPosition cameraPosition = new CameraPosition.Builder().target(LOCALIZACAO).zoom(15).build();
     
    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
 
    
            // check if map is created successfully or not
            if (googleMap == null) {
                Toast.makeText(getApplicationContext(),
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        initilizeMap();
    }
    
    public void gps(View view){

    	Intent intent2 = new Intent(android.content.Intent.ACTION_VIEW, 
				Uri.parse("geo:0,0?q="+ lat + "," + lon + " (" + nome + ")"));
				startActivity(intent2);
    }
}