package com.jorge.querofestas;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.jorge.model.Evento;

public class FestaDetalhesActivity extends Activity {
	Evento evento;
	static String nome;
	static String data;
	String info;
	String atracoes;
	String horario;
	static String cidade;
	String lat;
	String lon;
	static String local;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_festa_detalhes);

		getFesta();
		carregarDados();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return false;
	}

	public void getFesta() {
		Bundle extras = getIntent().getExtras();
		nome = extras.getString("nome");
		data = extras.getString("data");
		info = extras.getString("informacoes");
		atracoes = extras.getString("atracoes");
		horario = extras.getString("horario");
		cidade = extras.getString("cidade");
		lat = extras.getString("lat");
		lon = extras.getString("lon");
		local = extras.getString("local");
	}

	public void carregarDados() {

		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/OpenSans-Regular.ttf");

		Typeface type2 = Typeface.createFromAsset(getAssets(),
				"fonts/OpenSans-Bold.ttf");

		TextView nomeT = (TextView) findViewById(R.id.nomeFestaG);
		nomeT.setTypeface(type2);
		TextView atracoesT = (TextView) findViewById(R.id.atracoes);
		atracoesT.setTypeface(type);
		TextView cidadeT = (TextView) findViewById(R.id.cidadeG);
		cidadeT.setTypeface(type);
		TextView dataT = (TextView) findViewById(R.id.dataG);
		dataT.setTypeface(type);
		TextView horarioT = (TextView) findViewById(R.id.hora);
		horarioT.setTypeface(type);
		TextView informacoesT = (TextView) findViewById(R.id.info);
		informacoesT.setTypeface(type);
		TextView localT = (TextView) findViewById(R.id.local);
		localT.setTypeface(type);

		TextView atracoesTV = (TextView) findViewById(R.id.textoatracoes);
		atracoesTV.setTypeface(type2);
		TextView cidadeTV = (TextView) findViewById(R.id.textcidade);
		cidadeTV.setTypeface(type2);
		TextView horaTV = (TextView) findViewById(R.id.texthora);
		horaTV.setTypeface(type2);
		TextView infoTV = (TextView) findViewById(R.id.textinfo);
		infoTV.setTypeface(type2);
		TextView localTV = (TextView) findViewById(R.id.textlocal);
		localTV.setTypeface(type2);

		nomeT.setText(nome);
		atracoesT.setText(atracoes);
		cidadeT.setText(cidade);
		dataT.setText(data);
		horarioT.setText(horario);
		informacoesT.setText(info);
		localT.setText(local);
	}

	public void mapa(View view) {

		if (Integer.parseInt(Build.VERSION.SDK) < 11) {
			Intent intent2 = new Intent(android.content.Intent.ACTION_VIEW,
					Uri.parse("geo:0,0?q=" + lat + "," + lon + " (" + nome
							+ ")"));
			startActivity(intent2);

		} else {
			Intent intent = new Intent(this, MapaActivity.class);
			intent.putExtra("local", local);
			intent.putExtra("nome", nome);
			intent.putExtra("lat", lat);
			intent.putExtra("lon", lon);
			startActivity(intent);
		}

	}

	public void compartilhar(View view){
		Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
		sharingIntent.setType("text/plain");
		String shareBody = "Essa eu vou, " +nome+" no "+local+" dia "+data+". #ReiDaBalada - http://bit.ly/1tt5vEd";
		sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
		startActivity(Intent.createChooser(sharingIntent, "Compartilhe o evento"));
	}
	
}
