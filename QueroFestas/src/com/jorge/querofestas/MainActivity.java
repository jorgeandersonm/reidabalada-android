package com.jorge.querofestas;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.Window;

public class MainActivity extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return false;
	}
	
	public void listar(View view){
		
		Intent intent = new Intent(getApplicationContext(), FestasActivity.class);
		
		startActivity(intent);
		this.onPause();
	}
	
	public void destroy(View view){
		stopService(getIntent());
	}

}
