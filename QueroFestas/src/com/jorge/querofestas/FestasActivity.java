package com.jorge.querofestas;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.Header;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.jorge.model.Evento;
import com.jorge.model.EventosJson;
import com.jorge.model.Local;
import com.jorge.util.AdapterListView;
import com.jorge.util.ConverteData;
import com.jorge.util.FestasPorData;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class FestasActivity extends Activity implements OnItemClickListener {
	private static ListView listView;
	private static List<Evento> festasObjeto;
	private static List<Local> locais;
	private AlertDialog levelDialog;
	private static AdapterListView adapterListView;
	private static ProgressBar progressBar;
	private static RelativeLayout layoutBusca;
	private static RelativeLayout layoutFiltro;
	private int TENTATIVAS = 5;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_festas);

		instanciarElementos();
		listView.setOnItemClickListener(this);
		listView.setVisibility(View.GONE);
		layoutBusca.setClickable(false);
		layoutFiltro.setClickable(false);
		getJson(getApplicationContext());
	}

	public void instanciarElementos() {
		festasObjeto = new ArrayList<Evento>();
		locais = new ArrayList<Local>();
		listView = (ListView) findViewById(R.id.listaViewFestas);
		progressBar = (ProgressBar) findViewById(R.id.progressBar);
		layoutBusca = (RelativeLayout) findViewById(R.id.relativeLayout2);
		layoutFiltro = (RelativeLayout) findViewById(R.id.relativeLayout1);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return false;
	}

	public void finishActivity() {
		finish();
	}

	private void erro() {
		progressBar.setVisibility(View.INVISIBLE);
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
		alertDialog.setTitle("Conexão falhou :(");
		alertDialog.setPositiveButton("OK", new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				finishActivity();
			}
		});
		alertDialog.show();
	}

	@Override
	public void onBackPressed() {
		finishActivity();
		super.onBackPressed();
	}

	public void filtrar(View view) {
		CharSequence[] items = { "Todas", "Hoje", "Amanhã", "Essa Semana" };
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Filtrar eventos");
		builder.setSingleChoiceItems(items, -1,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int item) {
						switch (item) {
						case 0:
							criarListView(festasObjeto, getApplicationContext());
							levelDialog.dismiss();
							break;

						case 1:
							Date dataHoje = new Date();
							String dataString = ConverteData
									.dataParaString(dataHoje);
							List<Evento> eventos;
							eventos = FestasPorData.filtrarFestasPorData(
									dataString, null, festasObjeto);
							criarListView(eventos, getApplicationContext());
							levelDialog.dismiss();
							break;

						case 2:
							Date dataHoje2 = new Date();
							dataHoje2.setDate(dataHoje2.getDate() + 1);
							String dataString2 = ConverteData
									.dataParaString(dataHoje2);
							List<Evento> festas2;
							festas2 = FestasPorData.filtrarFestasPorData(
									dataString2, null, festasObjeto);
							criarListView(festas2, getApplicationContext());
							levelDialog.dismiss();
							break;

						case 3:
							Date dataHoje3 = new Date();
							Date dataSemana3 = new Date();
							dataSemana3.setDate(dataSemana3.getDate() + 6);
							String dataInicio = ConverteData
									.dataParaString(dataHoje3);
							String dataFim = ConverteData
									.dataParaString(dataSemana3);
							List<Evento> festas3;
							festas3 = FestasPorData.filtrarFestasPorData(
									dataInicio, dataFim, festasObjeto);
							criarListView(festas3, getApplicationContext());
							levelDialog.dismiss();
							break;
						}
					}
				});
		levelDialog = builder.create();
		levelDialog.show();
	}

	public void buscar(View view) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Buscar Festas");
		builder.setIcon(R.drawable.search);
		final EditText input = new EditText(this);
		input.setInputType(InputType.TYPE_CLASS_TEXT
				| InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
		builder.setView(input);
		builder.setPositiveButton("Buscar",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						String busca = input.getText().toString();
						List<Evento> eventos = new ArrayList<Evento>();

						for (int i = 0; i < festasObjeto.size(); i++) {
							if (festasObjeto.get(i).getLocal().getCidade()
									.contains(busca)
									|| festasObjeto.get(i).getAtracoes()
											.contains(busca)
									|| festasObjeto.get(i).getNome()
											.contains(busca)) {
								eventos.add(festasObjeto.get(i));
							}
						}
						criarListView(eventos, getApplicationContext());
					}
				});

		builder.setNegativeButton("Cancelar",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
		builder.show();

	}

	public static void criarListView(List<Evento> listFestas, Context context) {
		adapterListView = new AdapterListView(context, listFestas);
		listView.setAdapter(adapterListView);
		listView.setCacheColorHint(Color.TRANSPARENT);
		progressBar.setVisibility(View.GONE);
		listView.setVisibility(View.VISIBLE);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		Evento item = adapterListView.getItem(arg2);

		Intent intent = new Intent(this, FestaDetalhesActivity.class);
		intent.putExtra("cidade", item.getLocal().getCidade());
		intent.putExtra("horario", item.getHorario());
		intent.putExtra("nome", item.getNome());
		intent.putExtra("atracoes", item.getAtracoes());
		intent.putExtra("data", item.getData());
		intent.putExtra("informacoes", item.getInformacoes());
		intent.putExtra("local", item.getLocal().getNome());
		intent.putExtra("lat", item.getLocal().getLat());
		intent.putExtra("lon", item.getLocal().getLon());

		for (int i = 0; i < locais.size(); i++) {
			if (locais.get(i).getId() == item.getLocal().getId()) {
				intent.putExtra("local", locais.get(i).getNome());
				intent.putExtra("lat", locais.get(i).getLat());
				intent.putExtra("lon", locais.get(i).getLon());
				break;
			}
		}
		startActivity(intent);
	}

	public void getJson(final Context context) {
		String urlString = "http://137.135.58.78:8080/gev/evento/json";

		System.out.println(urlString);
		AsyncHttpClient client = new AsyncHttpClient();
		client.get(urlString, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String response) {				
				Gson gson = new Gson();

				EventosJson listFestas = gson.fromJson(response, EventosJson.class);
				festasObjeto = listFestas.getEventos();
				if (festasObjeto == null) {
					erro();
				} else {
					layoutBusca.setClickable(true);
					layoutFiltro.setClickable(true);
					criarListView(festasObjeto, context);
				}
			}

			@Override
			public void onFailure(int statusCode, Header[] headers,
					byte[] responseBody, Throwable error) {
				--TENTATIVAS;
				if(TENTATIVAS > 0){
					getJson(getApplicationContext());
				}
				else{
				festasObjeto = null;
				System.out.println("FALHA");
				erro();
			}}
		});
	}
}